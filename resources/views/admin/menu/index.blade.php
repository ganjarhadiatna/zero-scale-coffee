@extends('layouts.admin')
@section('content')
<?php 
    use Adventrest\TagModel; 
?>
<script>
    var server = '{{ url("/") }}';
    
    function deleteMenu ($idmenu) 
    { 
        var a = confirm('Delete this menu?');
        if (a == true) {
            $.ajax({
                url: '{{ url("/admin/menu/remove") }}',
                dataType: 'json',
                type: 'post',
                data: {
                    'idmenu': $idmenu
                },
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = server+'/admin/menu';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
                alert(data.responseJSON.message);
			   	//console.log(data.responseJSON);
			})
			.always(function () {
				//after done
			});
        }
    }
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Pengelolaan</p>
    <h1>Menu</h1>
    <div class="bdr"></div>
    <div class="padding-10px"></div>
    <p>
        Masukan gambar-gambar unik yang kamu miliki
    </p>
</div>

<div class="content-page">
    <div class="cp-top">
        <div class="cp-left">
            <a href="{{ url('/admin/menu/create') }}">
                <button class="btn btn-main-color btn-radius">
                    <span class="fa fa-lg fa-plus"></span>
                    <span>Buat Menu</span>
                </button>
            </a>
        </div>
        <div class="cp-right">
            <form action="#">
                <div class="search">
                    <input 
                        type="text" 
                        class="src txt txt-main-color" 
                        placeholder="Search..." 
                        required="required">
                    <button class="bt btn btn-main-color" type="submit">
                        <span class="fa fa-lg fa-search"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="cp-mid">
        <table>
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th width="100">Gambar</th>
                    <th width="200">Title</th>
                    <th width="200" class="mobile">price</th>
                    <th width="150" class="mobile">Tanggal</th>
                    <th width="100"></th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach ($menu as $gl)
                <tr>
                    <td><strong>{{ $i }}</strong></td>
                    <td>
                        <div 
                            class="image image-100px" 
                            style="
                                background-image: url({{ asset('/img/menu/thumbnails/'.$gl->cover) }});
                            "></div>
                    </td>
                    <td class="mobile">{{ $gl->title }}</td>
                    <td class="mobile">{{ $gl->price }}</td>
                    <td class="mobile">{{ $gl->date }}</td>
                    <td>
                        <a href="{{ url('/admin/menu/edit/'.$gl->idmenu) }}">
                            <button class="btn btn-sekunder-color btn-circle">
                                <span class="fa fa-1x fa-pencil-alt"></span>
                            </button>
                        </a>
                        <button 
                            class="btn btn-sekunder-color btn-circle"
                            onclick="deleteMenu('{{ $gl->idmenu }}')">
                            <span class="fa fa-1x fa-trash-alt"></span>
                        </button>
                    </td>
                </tr>
                <?php $i++ ?>
                @endforeach
            </tbody>
        </table>
        <div>
            {{ $menu->links() }}
        </div>
    </div>
</div>
@endsection
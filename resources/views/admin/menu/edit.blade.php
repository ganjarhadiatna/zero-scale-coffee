@extends('layouts.admin')
@section('content')
<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();

        var ctn = 1;
        var idmenu = $('#idmenu').val();
        var title = $('#title').val();
        var description = $('#description').val();
        var price = $('#price').val();

        if (ctn > 0) {
            fd.append('idmenu', idmenu);
            fd.append('title', title);
            fd.append('description', description);
            fd.append('price', price);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ url("/admin/menu/put") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = server+'/admin/menu';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
			   	alert(data.responseJSON.message);
			})
			.always(function () {
				//loadPopup('hide');
			});
        } else {
            alert('Please select one cover');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Menu</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        <div class="cc-left">
        @foreach ($menu as $gl)
            <input type="hidden" id="idmenu" value="{{ $gl->idmenu }}">

            <div class="cc-block">
                <div class="label">
                    Cover
                </div>
                <div class="desc">
                    <p>Gambar tidak bisa dirubah</p>
                </div>
                <div 
                    class="image image-150px"
                    style="background-image: url({{ asset('/img/menu/thumbnails/'.$gl->cover) }});"></div>
            </div>

            <div class="cc-block">
                <div class="label">
                    Judul
                </div>
                <input 
                    type="text"
                    name="title"
                    id="title"
                    class="txt txt-primary-color"
                    value="{{ $gl->title }}"
                    required>
            </div>

            <div class="cc-block">
                <div class="label">
                    Deskripsi (opsional)
                </div>
                <div class="desc">
                    Jelaskan tentang galeri yang diupload, gunakan kalimat yang jelas,
                    tidak berbelit-belit dan langsung ke poko bahasan.
                </div>
                <textarea 
                    name="deskripsi" 
                    id="description"
                    class="txt txt-primary-color edit-text">{{ $gl->description }}</textarea>
            </div>

            <div class="cc-block">
                <div class="label">
                    Price (opsional)
                </div>
                <div class="desc">
                    IDR
                </div>
                <input 
                    type="text"
                    name="price"
                    id="price"
                    class="txt txt-primary-color"
                    value="{{ $gl->price }}"
                    placeholder="">
            </div>
        @endforeach
        </div>
        <div class="cc-right">
            <div class="cc-block bdr-all">
                <div class="label">
                    Catatan
                </div>
                <ul class="cc-note">
                    <li>Jika deskripsi dikosongkan maka yang dimunculkan hanya gambar saja</li>
                    <li>Deskripsi bertipe opsional artinya boleh diisi boleh juga tidak</li>
                </ul>
            </div>
            <div class="cc-block">
                <input 
                    type="submit" 
                    value="Simpan"
                    class="btn btn-main-color">
            </div>
            <div class="cc-block">
                <input 
                    type="button" 
                    value="Cancel"
                    onclick="goBack()" 
                    class="btn btn-sekunder-color">
            </div>
        </div>
    </div>
</form>

@endsection
@extends('layouts.admin')
@section('content')
<?php 
    use Adventrest\TagModel; 
?>
<script>
    var server = '{{ url("/") }}';
    
    function deleteVideo ($idvideo) 
    { 
        var a = confirm('Delete this video?');
        if (a == true) {
            $.ajax({
                url: '{{ url("/admin/video/remove") }}',
                dataType: 'json',
                type: 'post',
                data: {
                    'idvideo': $idvideo
                },
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = server+'/admin/video';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
                alert(data.responseJSON.message);
			   	//console.log(data.responseJSON);
			})
			.always(function () {
				//after done
			});
        }
    }
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Pengelolaan</p>
    <h1>Video</h1>
    <div class="bdr"></div>
    <div class="padding-10px"></div>
    <p>
        Masukan video-video unik yang kamu miliki
    </p>
</div>

<div class="content-page">
    <div class="cp-top">
        <div class="cp-left">
            <a href="{{ url('/admin/video/create') }}">
                <button class="btn btn-main-color btn-radius">
                    <span class="fa fa-lg fa-plus"></span>
                    <span>Buat Video</span>
                </button>
            </a>
        </div>
        <div class="cp-right">
            <form action="#">
                <div class="search">
                    <input 
                        type="text" 
                        class="src txt txt-main-color" 
                        placeholder="Search..." 
                        required="required">
                    <button class="bt btn btn-main-color" type="submit">
                        <span class="fa fa-lg fa-search"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="cp-mid">
        <table>
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th width="100">Gambar</th>
                    <th width="200">Video</th>
                    <th class="mobile">Deskripsi</th>
                    <th width="150" class="mobile">Tanggal</th>
                    <th width="100"></th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach ($video as $gl)
                <tr>
                    <td><strong>{{ $i }}</strong></td>
                    <td>
                        <div 
                            class="image image-100px" 
                            style="
                                background-image: url({{ asset('/vid/thumbnails/'.$gl->cover) }});
                            "></div>
                    </td>
                    <td>
                        <video width="200" height="200" controls>
                            <source src="{{ asset('/vid/'.$gl->video) }}">
                        </video>
                    </td>
                    <td class="mobile">{{ $gl->description }}</td>
                    <td class="mobile">{{ $gl->date }}</td>
                    <td>
                        <a href="{{ url('/admin/video/edit/'.$gl->idvideo) }}">
                            <button class="btn btn-sekunder-color btn-circle">
                                <span class="fa fa-1x fa-pencil-alt"></span>
                            </button>
                        </a>
                        <button 
                            class="btn btn-sekunder-color btn-circle"
                            onclick="deleteVideo('{{ $gl->idvideo }}')">
                            <span class="fa fa-1x fa-trash-alt"></span>
                        </button>
                    </td>
                </tr>
                <?php $i++ ?>
                @endforeach
            </tbody>
        </table>
        <div>
            {{ $video->links() }}
        </div>
    </div>
</div>
@endsection
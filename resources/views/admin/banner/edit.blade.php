@extends('layouts.admin')
@section('content')

<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();
        
        var idbanner = $('#idbanner').val();
        var title = $('#title').val();
		var description = $('#description').val();
        var link = $('#link').val();
        var position = $('#position').val();
        //var cover = $('#cover')[0].files[0];
		var ctn = 1; //$('#cover')[0].files.length;

        if (ctn > 0) {
            fd.append('idbanner', idbanner);
            fd.append('title', title);
            fd.append('description', description);
            fd.append('link', link);
            fd.append('position', position);
            //fd.append('cover', cover);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ url("/admin/banner/put") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = server+'/admin/banner';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
                alert(data.responseJSON.message);
			   	//console.log(data.responseJSON);
			})
			.always(function () {
				//after done
			});
        } else {
            alert('Please select one cover');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Banner</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    @foreach ($banner as $bn)
        <div class="content-create">
            <div class="cc-left">

                <input type="hidden" id="idbanner" value="{{ $bn->idbanner }}">

                <div class="cc-block">
                    <div class="label">
                        Cover
                    </div>
                    <div 
                        class="image image-150px"
                        style="
                            background-image: url('{{ asset('img/banner/thumbnails/'.$bn->cover) }}')
                        "></div>
                </div>
                <div class="cc-block">
                    <div class="label">
                        Judul (opsional)
                    </div>
                    <div class="desc">
                        <p>Gunakan kalimat yang jelas dan tidak terlalu panjang</p>
                    </div>
                    <input 
                        type="text"
                        name="title"
                        id="title"
                        class="txt txt-primary-color"
                        value="{{ $bn->title }}"
                        placeholder="">
                </div>
                <div class="cc-block">
                    <div class="label">
                        Deskripsi (opsional)
                    </div>
                    <div class="desc">
                        <p>
                            Jelaskan tentang banner yang diupload, gunakan kalimat yang jelas,
                            tidak berbelit-belit dan langsung ke poko bahasan.
                        </p>
                    </div>
                    <div class="padding-bottom-5px">
                        <p class="ctn-main-font ctn-14px ctn-sek-color">
                            <span id="desc-length">0</span>/250
                        </p>
                    </div>
                    <textarea 
                        name="description"
                        id="description"
                        class="txt txt-primary-color edit-text"
                        maxlength="250">{{ $bn->description }}</textarea>
                </div>
                <div class="cc-block">
                    <div class="label">
                        Link (opsional)
                    </div>
                    <div class="desc">
                        <p>
                            Bisa link artikel yang sudah dibuat ataupun link lainnya.
                        </p>
                    </div>
                    <input 
                        type="link"
                        name="link"
                        class="txt txt-primary-color"
                        id="link"
                        value="{{ $bn->link }}"
                        placeholder="">
                </div>
                <div class="cc-block">
                    <div class="label">
                        Posisi Text
                    </div>
                    <div class="desc">
                        <p>
                            Posisi dari judul, deskripsi dan link banner.
                        </p>
                    </div>
                    @if ($bn->position == 'center')
                        <?php
                            $rq_center = 'selected="true"';
                            $rq_bottom = '';
                            $rq_disable = '';
                        ?>
                    @endif
                    @if ($bn->position == 'bottom')
                        <?php
                            $rq_center = '';
                            $rq_bottom = 'selected="true"';
                            $rq_disable = '';
                        ?>
                    @endif
                    @if ($bn->position == 'disable')
                        <?php
                            $rq_center = '';
                            $rq_bottom = '';
                            $rq_disable = 'selected="true"';
                        ?>
                    @endif
                    <select class="slc" style="width: 150px;" name="position" id="position">
                        <option value="center" {{ $rq_center }}>Tengah</option>
                        <option value="bottom" {{ $rq_bottom }}>Bawah</option>
                        <option value="disable" {{ $rq_disable }}>Sembunyikan</option>
                    </select>
                </div>
            </div>
            <div class="cc-right">
                <div class="cc-block bdr-all">
                    <div class="label">
                        Catatan
                    </div>
                    <ul class="cc-note">
                        <li>Gambar banner tidak bisa dirubah</li>
                        <li>Jika judul, deskripsi dan link dikosongkan maka yang dimunculkan hanya gambar saja</li>
                        <li>Judul, deskripsi dan link bertipe opsional artinya boleh diisi boleh juga tidak</li>
                    </ul>
                </div>
                <div class="cc-block">
                    <input 
                        type="submit" 
                        value="Simpan"
                        class="btn btn-main-color">
                </div>
                <div class="cc-block">
                    <input 
                        type="button" 
                        value="Cancel"
                        onclick="goBack()" 
                        class="btn btn-sekunder-color">
                </div>
            </div>
        </div>
    @endforeach
</form>

@endsection
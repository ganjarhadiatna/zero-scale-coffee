@extends('layouts.admin')
@section('content')
<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();

        var idnote = $('#idnote').val();
        var icon = $('#icon').val();
        var title = $('#title').val();
        var description = $('#description').val();
        var link = $('#link').val();
		var ctn = 1;

        if (ctn > 0) {
            fd.append('idnote', idnote);
            fd.append('icon', icon);
            fd.append('title', title);
            fd.append('description', description);
            fd.append('link', link);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ url("/admin/note/put") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = server+'/admin/note';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                    
                }
			})
			.fail(function(data) {
                loadPopup('hide');
			   	alert(data.responseJSON.message);
			})
			.always(function () {
				//after done
			});
        } else {
            alert('Please set all field');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Catatan & Hal Penting</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        <div class="cc-left">
        @foreach ($note as $sv)
            <input type="hidden" id="idnote" value="{{ $sv->idnote }}">
            <div class="cc-block">
                <div class="label">
                    Ikon
                </div>
                <div class="desc">
                    <p>Contoh: "fa fa-lg fa-heart"</p>
                </div>
                <input 
                    type="text"
                    name="icon"
                    id="icon"
                    class="txt txt-primary-color"
                    value="{{ $sv->icon }}"
                    required="required"
                    placeholder="fa fa-lg fa-nama-icon">
            </div>
            <div class="cc-block">
                <div class="label">
                    Judul
                </div>
                <div class="desc">
                    <p>Gunakan kalimat yang jelas dan tidak terlalu panjang</p>
                </div>
                <input 
                    type="text"
                    name="title"
                    id="title"
                    class="txt txt-primary-color"
                    value="{{ $sv->title }}"
                    required="required"
                    placeholder="">
            </div>
            <div class="cc-block">
                <div class="label">
                    Deskripsi (opsional)
                </div>
                <div class="desc">
                    <p>
                        Gunakan kalimat yang jelas,
                        tidak berbelit-belit dan langsung ke poko bahasan.
                    </p>
                </div>
                <div class="padding-bottom-5px">
                    <p class="ctn-main-font ctn-14px ctn-sek-color">
                        <span id="desc-length">0</span>/250
                    </p>
                </div>
                <textarea 
                    name="deskripsi" 
                    id="description"
                    maxlength="250"
                    class="txt txt-primary-color edit-text">{{ $sv->description }}</textarea>
            </div>
            <div class="cc-block">
                <div class="label">
                    Link
                </div>
                <div class="desc">
                    <p>Gunakan link dari artikel ataupun tag dari galeri</p>
                </div>
                <input 
                    type="text"
                    name="link"
                    id="link"
                    value="{{ $sv->link }}" 
                    class="txt txt-primary-color"
                    placeholder="https://">
            </div>
        @endforeach
        </div>
        <div class="cc-right">
            <div class="cc-block bdr-all">
                <div class="label">
                    Catatan
                </div>
                <ul class="cc-note">
                    <li>
                        Ikon disini berupa kode icon yang bisa didapatkan di
                        <a href="https://fontawesome.com/icons?d=gallery" target="_blank">
                            link ini
                        </a>
                    </li>
                    <li>Deskripsi bertipe opsional artinya boleh diisi boleh juga tidak</li>
                </ul>
            </div>
            <div class="cc-block">
                <input 
                    type="submit" 
                    value="Simpan"
                    class="btn btn-main-color">
            </div>
            <div class="cc-block">
                <input 
                    type="button" 
                    value="Cancel"
                    onclick="goBack()" 
                    class="btn btn-sekunder-color">
            </div>
        </div>
    </div>
</form>

@endsection
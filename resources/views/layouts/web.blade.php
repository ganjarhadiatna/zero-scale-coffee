<?php 
    use Adventrest\ServiceModel; 
    use Adventrest\NoteModel; 
?>
<?php
    if (isset($path)) {
        $nowPath = '#'.$path;
    } else {
        $nowPath = '';
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    
    <!--meta-->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="google-site-verification" content="7gjYBIjkR1XNmK_TnKMlOb37ukFqbMSPlBA8r_wHHD4" />

    <meta name = "format-detection" content = "telephone=no" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="generator" content="" />
    <meta name="robots" content="index,follow" />

    <link rel="shortcut icon" href="{{ asset('/img/sites/favicon.ico') }}">

    <!--css-->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('icons/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('scss/app.css') }}" />

    <!--js-->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127323790-1"></script>
    <script type="text/javascript"> //<![CDATA[ 
    var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
    document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
    //]]>
    </script>
    -->
    <script>
        /*window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127323790-1');*/

        function showSubMenu(nav) {
            var tr = $('#'+nav).attr('class');
            if (tr == 'sb-menu') {
                $('#'+nav).addClass('sb-open');
                //$('#'+nav).hide();
                //$('#'+nav).css('display', 'hide');
            } else {
                $('#'+nav).removeClass('sb-open');
                //$('#'+nav).show();
                //$('#'+nav).css('display', 'block');
            }
        }
        function opBar(stt) {  
            if (stt == 'show') {
                //$('#opBar').animate({right: 0}, 500);
                $('#opBar').addClass('pl-active');
            } else {
                //$('#opBar').animate({right: -350}, 500);
                $('#opBar').removeClass('pl-active');
            }
        }
        function toLeft(path) {
			var wd = $('#'+path).width();
			var sc = $('#'+path).scrollLeft();
			if (sc >= 0) {
				$('#'+path).animate({scrollLeft: (sc - wd)}, 500);
			}
		}
		function toRight(path) {
			var wd = $('#'+path).width();
			var sc = $('#'+path).scrollLeft();
			if (true) {
				$('#'+path).animate({scrollLeft: (sc + wd)}, 500);
			}
		}
        $(document).ready(function () {
            $('#toTop').on('click', function () {
                $('body, html').animate({scrollTop: 0}, 500);
            });

            $('{{ $nowPath }}').addClass('active');
            
            $(window).scroll(function(event) {
                var top = $(this).scrollTop();
                var hg = 300;
                if (top > hg) {
                    $('#plToTop').fadeIn();
                } else {
                    $('#plToTop').fadeOut();
                }
            });
        });
    </script>

</head>
<body>
    <div class="header">
        <div class="place" id="pl-header">
            <div class="menu">
                
                <div class="col col-1">
                    <div class="pl-logo">
                        <a href="{{ url('/') }}">
                            <img 
                                src="{{ asset('img/sites/logo.png') }}" 
                                class="logo">
                        </a>
                    </div>
                </div>

                <div class="col col-2">
                    <div class="pl-bar">
                        <ul class="mn-menu">
                            <li class="mn-list mn-bar" id="bar" onclick="opBar('show')">
                                <span class="fa fa-lg fa-bars"></span>
                            </li>
                        </ul>
                    </div>
                    <div class="pl-menu" id="opBar">
                        <ul class="mn-menu nav">
                            <li class="mn-list mn-close">
                                <span class="icn fa fa-lg fa-times" onclick="opBar('hide')"></span>
                            </li>
                        </ul>

                        <ul class="mn-menu">
                            <li class="mn-list" id="home">
                                <a class="mn-link" href="{{ url('/') }}">
                                    Home Page
                                </a>
                            </li>
                            {{-- <li class="mn-list" id="service">
                                <a class="mn-link" href="javascript:void(0)" onclick="showSubMenu('head-service')">
                                    <span>Our Services</span>
                                    <span class="fa fa-lg fa-angle-down"></span>
                                </a>
                                <ul class="sb-menu" id="head-service">
                                    @foreach (ServiceModel::AllService(10) as $sv)
                                        <li class="sb-list">
                                            @if ($sv->link == '')
                                                <div class="sb-link">
                                                    <span class="icn fa fa-lg {{ $sv->icon }}"></span>
                                                    <span class="ttl">{{ $sv->title }}</span>
                                                </div>
                                            @else
                                                <a class="sb-link" href="{{ $sv->link }}">
                                                    <span class="icn fa fa-lg {{ $sv->icon }}"></span>
                                                    <span class="ttl">{{ $sv->title }}</span>
                                                </a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li> --}}
                            <li class="mn-list" id="galery">
                                <a class="mn-link" href="{{ url('/galeries') }}">
                                    Activities & Galeries
                                </a>
                            </li>
                            <li class="mn-list" id="video">
                                <a class="mn-link" href="{{ url('/videos') }}">
                                    Video Collections
                                </a>
                            </li>
                            <li class="mn-list" id="menu">
                                <a class="mn-link" href="{{ url('/menus') }}">
                                    Popular Menus
                                </a>
                            </li>
                            <li class="mn-list" id="article">
                                <a class="mn-link" href="{{ url('/articles') }}">
                                    News & Blogs
                                </a>
                            </li>
                            <li class="mn-list" id="information">
                                <a class="mn-link" href="javascript:void(0)" onclick="showSubMenu('head-info')">
                                    <span class="fa fa-lg fa-ellipsis-h"></span>
                                </a>
                                <ul class="sb-menu" id="head-info">
                                    <li class="sb-list" id="information">
                                        <a class="sb-link" href="{{ url('/sites/about-us') }}">
                                            About Us
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/terms-n-conditions') }}">
                                            Terms & Conditions
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/privacy') }}">
                                            Privacy
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/faq') }}">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mn-list mn-search" id="search" onclick="opSearch('show')">
                                <span class="fa fa-lg fa-search"></span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('web.main.map-popup')
    @include('web.main.video-popup')
    @include('web.main.search-popup')
    @include('web.main.image-popup')

    <div class="pin" id="plToTop" style="display: none;">
        <div class="ctn" id="toTop">
            <div class="icn fa fa-lg fa-arrow-up"></div>
        </div>
    </div>

    <div class="body">
        @yield('content')
    </div>

    <div class="footer">
        <div class="copy">
            <div>
                Designed and powered by
                <strong>CocaCode</strong> --
                All Rights Reserved |
                <span class="fa fa-1x fa-copyright"></span>
                2018
                {{ config('app.name') }}
            </div>
            <div>
                <ul class="menu">
                    <li>
                        <a href="https://www.instagram.com/ganjar_hadiatna" target="_blank">
                            <span class="icn fab fa-lg fa-instagram"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/" target="_blank">
                            <span class="icn fab fa-lg fa-facebook-f"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/" target="_blank">
                            <span class="icn fab fa-lg fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        
                    </li>
                </ul>
            </div>
        </div>

    </div>
</body>
</html>
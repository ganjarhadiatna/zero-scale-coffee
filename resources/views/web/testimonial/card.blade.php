<div class="frm-testimonial">
	<div class="ft-grid">
		<div class="ft-title">
			<div class="ft-top">
				<div 
					class="image image-all image-circle" 
					style="background-image: url({{ asset('/img/testimony/thumbnails/'.$tt->photo) }}"></div>
			</div>
		</div>
		<div class="ft-info">
			<div class="ft-mid">
				<h2 class="ttl ctn-main-font ctn-font-2 ctn-thin">{{ $tt->name }}</h2>
				<p class="subttl ctn-main-font ctn-font-3 ctn-bold ctn-12pt">{{ $tt->job }}</p>
			</div>
			<div class="ft-ctn">
				{{ $tt->response }}
			</div>
		</div>
	</div>
</div>
@extends('layouts.web')
@section('title', $title)
@section('content')
<div class="body-padding"></div>
<div class="padding-20px">
	<div class="padding-15px"></div>
	<div class="informations">
		<h1 class="ctn-main-font ctn-mikro ctn-sek-color ctn-font-2 ctn-thin ctn-center ctn-line">
			Terms & Conditions
		</h1>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
	</div>
	<div class="padding-15px"></div>
</div>
@endsection
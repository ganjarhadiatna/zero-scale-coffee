<?php $path = asset('/img/galery/covers/'.$gl->cover); ?>
<div 
	onclick="opViewImage('{{ $gl->idgalery }}', '{{ $path }}')" 
	class="frm-galery" >
	<div class="image" style="background-image: url({{ asset('/img/galery/thumbnails/'.$gl->cover) }});"></div>
    <div class="desc">
    	<div class="ttl">{{ $gl->description }}</div>
    </div>
</div>
@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

@foreach ($detail as $gl)
	<div class="frm-view-galery">
        <div class="frm-place">
    		<div class="top">
    			<img src="{{ asset('/img/galery/covers/'.$gl->cover) }}">
    		</div>
    		<div class="mid">
    			<p class="ctn-main-font ctn-font-2 ctn-16pt ctn-thin ctn-white-color">
    				{{ $gl->description }}
    			</p>
    		</div>
            <div class="bot">
                @foreach ($tags as $tg)
                    <div class="frm-tags">
                        <a href="{{ url('/galeries/tags/'.strtolower($tg->tag)) }}">
                            {{ $tg->tag }}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
	</div>
@endforeach

<div class="body-block">
	<div class="mid">
        <div class="place-more">
            <div class="cen" id="gc-1">
            	@foreach ($galery as $gl)
            		@include('web.galery.card')
            	@endforeach
            </div>
        </div>
    </div>
    <div class="bot padding-top-10px">
        <div>
        	{{ $galery->links() }}
        </div>
    </div>
</div>

@endsection
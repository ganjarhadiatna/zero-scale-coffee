<?php $path = asset('/img/menu/covers/'.$tt->cover); ?>
<div class="frm-menu">
	<div 
		class="ft-top" 
		style="cursor: pointer;"
		onclick="opViewImage('{{ $tt->idmenu }}', '{{ $path }}')">
		<div 
			class="image image-all image-circle" 
			style="background-image: url({{ asset('/img/menu/thumbnails/'.$tt->cover) }}"></div>
	</div>
	<div class="ft-mid">
		<h2 class="ctn-main-font ctn-font-2 ctn-init-color ctn-thin">
			{{ $tt->price }}
		</h2>
		{{-- <p class="subttl ctn-main-font ctn-font-3 ctn-thin ctn-12pt">{{ $tt->job }}</p> --}}
    </div>
    <div class="ft-ctn">
        <h3 class="ctn-main-font ctn-14pt ctn-bold">
            {{ $tt->title }}
        </h3>
		<p>
            {{ $tt->description }}
        </p>
	</div>
	<div class="ft-bot">
		<div class="icn fab fa-lg fa-instagram"></div>
		<div class="icn fab fa-lg fa-twitter"></div>
	</div>
</div>
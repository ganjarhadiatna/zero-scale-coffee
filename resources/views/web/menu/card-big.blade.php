<?php $path = asset('/img/menu/covers/'.$tt->cover); ?>
<div class="frm-galery" >
	<div 
		onclick="opViewImage('{{ $tt->idmenu }}', '{{ $path }}')"
		class="image" 
		style="background-image: url({{ asset('/img/menu/thumbnails/'.$tt->cover) }});"></div>
    <div class="desc">
    	<div class="ttl">
			<h3 class="ctn-main-font ctn-14pt ctn-init-color">{{ $tt->price }}</h3>
			<div class="margin-10px">
				<h4 class="ctn-main-font ctn-12pt ctn-min-color">{{ $tt->title }}</h4>
				<p class="ctn-main-font ctn-11pt ctn-sek-color">{{ $tt->description }}</p>
			</div>
        </div>
	</div>
</div>
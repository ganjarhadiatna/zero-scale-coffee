<div class="frm-service">
    <div class="">
        @if ($sv->link == '')
            <div class="icn">
                <span class="fl {{ $sv->icon }}"></span>
            </div>
        @else
            <a href="{{ $sv->link }}" class="icn-link">
                <div class="icn">
                    <span class="fl {{ $sv->icon }}"></span>
                </div>
            </a>
        @endif
        <div class="ctn">
            @if ($sv->link == '')
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-init-color">
                    {{ $sv->title }}
                </div>
            @else
                <div>
                    <a 
                        href="{{ $sv->link }}" 
                        class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-init-color">
                        {{ $sv->title }}
                    </a>
                </div>
            @endif
            <div class="dsc">
                {{ $sv->description }}
            </div>
            @if ($sv->link != '')
                <div class="more">
                    <a href="{{ $sv->link }}">
                        <button class="btn btn-radius btn-sekunder-color btn-circle btn-circle-small">
                            <span class="fa fa-1x fa-plus"></span>
                        </button>
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>
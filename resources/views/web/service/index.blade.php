@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    @if (count($article) != 0)
        <div class="top">
        @foreach ($service as $sv)
            <p class="desc">Contents & Services</p>

            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                {{ str_replace('_', ' ', $sv->title) }}
            </h1>

            <div class="bdr"></div>
            <div class="padding-5px"></div>

            <div class="width">
                <p class="desc text-normal">{{ $sv->description }}</p>
            </div>
        @endforeach
        </div>
        <div class="mid">
                
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($article as $at)
                        @include('web.article.card')
                    @endforeach
                </div>
            </div>

        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $article->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                Contents & Services
            </p>
            @foreach ($service as $sv)
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                    {{ str_replace('_', ' ', $sv->title) }}
                </h1>
            @endforeach
            <div class="bdr"></div>
            <div class="padding-5px"></div>

            <div class="width">
                <p class="desc text-normal">{{ $sv->description }}</p>
            </div>
        </div>
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Artikel masih kosong
            </h1>
        </div>
    @endif
</div>

@endsection
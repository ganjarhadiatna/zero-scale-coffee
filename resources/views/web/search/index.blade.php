@extends('layouts.web')
@section('title', $title)
@section('content')

<?php 
    $src = $_GET['src'];
    $trg = $_GET['nav'];
    if ($trg == 'video') {
        $nav = '#nav-video';
    }
    if ($trg == 'menu') {
        $nav = '#nav-menu';
    }
    if ($trg == 'article') {
        $nav = '#nav-article';
    }
    if ($trg == 'galery') {
        $nav = '#nav-galery';
    }
?>

<script>
    $(document).ready(function () {
        $('{{ $nav }}').addClass('active');
    });
</script>

<div class="body-padding"></div>
<div class="body-block">
    <div class="top padding-20px">
        <div class="padding-10px"></div>
        <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
            Searching Results
        </p>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color ctn-capital">
            Key words "{{ $src }}"
        </h1>
    </div>
    <div>
        <div class="padding-10px"></div>
        
        <div class="padding-bottom-20px">
            <ul class="navigator">
                <a href="{{ url('/search?nav=galery&src='.$src) }}">
                    <li class="left" id="nav-galery">Galeries</li>
                </a>
                <a href="{{ url('/search?nav=video&src='.$src) }}">
                    <li class="middle" id="nav-video">Videos</li>
                </a>
                <a href="{{ url('/search?nav=menu&src='.$src) }}">
                    <li class="middle" id="nav-menu">Menus</li>
                </a>
                <a href="{{ url('/search?nav=article&src='.$src) }}">
                    <li class="right" id="nav-article">Articles</li>
                </a>
            </ul>
        </div>

        <div class="padding-10px"></div>

        <div class="mid center">
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @if ($trg == 'galery')
                        @if (count($search) != 0)
                            @foreach ($search as $gl)
                                @include('web.galery.card-big')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Galeries Not Found
                                </h1>
                            </div>
                        @endif
                    @endif
                    @if ($trg == 'video')
                        @if (count($search) != 0)
                            @foreach ($search as $vid)
                                @include('web.video.card-big')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Videos Not Found
                                </h1>
                            </div>
                        @endif
                    @endif
                    @if ($trg == 'menu')
                        @if (count($search) != 0)
                            @foreach ($search as $tt)
                                @include('web.menu.card-big')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Menus Not Found
                                </h1>
                            </div>
                        @endif
                    @endif
                    @if ($trg == 'article')
                        @if (count($search) != 0)
                            @foreach ($search as $at)
                                @include('web.article.card')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Articles Not Found
                                </h1>
                            </div>
                        @endif
                    @endif                    
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $search->links() }}
            </div>
        </div>
        <div class="padding-20px"></div>
    </div>
</div>

@endsection
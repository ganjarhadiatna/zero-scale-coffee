<div class="frm-article">
	<a href="{{ url('/article/'.base64_encode($at->idarticle)) }}">
		<div class="cover big">
            <div 
                class="image" 
                style="background-image: url({{ asset('/img/article/thumbnails/'.$at->cover) }}"></div>
		</div>
	</a>
	<div class="content big">
		<div class="date">
			<span class="icn fa fa-lg fa-clock"></span>
			<span>{{ date_format(date_create($at->date), "M d, Y") }}</span>
			<span class="icn fa fa-lg fa-user"></span>
			<span>{{ $at->name }}</span>
		</div>
		<div class="ttl">
			<h1 class="ctn-main-font ctn-16pt ctn-sek-color">
				<a href="{{ url('/article/'.base64_encode($at->idarticle)) }}">
					{{ $at->title }}
				</a>
			</h1>
		</div>
		<div class="more">
			<a href="{{ url('/article/'.base64_encode($at->idarticle)) }}">
				<button class="btn btn-sekunder-color btn-radius">
					Read More
					<span class="fa fa-lg fa-angle-right"></span>
				</button>
			</a>
		</div>
	</div>
</div>
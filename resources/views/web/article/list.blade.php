@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top padding-20px">
        <div class="padding-10px"></div>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color">
            News & blogs
        </h1>
    </div>
    @if (count($article) != 0)
        <div class="mid center">
                
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($article as $at)
                        @include('web.article.card')
                    @endforeach
                </div>
            </div>

        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $article->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                articles still empty
            </h1>
        </div>
    @endif
    <div class="padding-20px"></div>
</div>

@endsection
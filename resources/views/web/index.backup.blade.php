@extends('layouts.web')
@section('title', $title)
@section('content')
<script>
var slideIndex = 0;
var scrollBefore = $('.bn-main').width();

function hdScroll() {
    var top = $(this).scrollTop();
    var hg = $('#banner').height() - $('#other').height();
    if (top > hg) {
        $('#pl-header').removeClass('hd-bg-trans');
    } else {
        $('#pl-header').addClass('hd-bg-trans');
    }
}

$(document).ready(function () {
    $('#banner').bxSlider({
        auto: true,
        autoHover: false,
        controls: false,
        autoControls: false,
        stopAutoOnClick: false,
        pager: true,
    });
    
    /*hdScroll();
    $(window).scroll(function(event) {
        hdScroll();
    });*/
});
</script>

<div class="body-padding"></div>

@include('web.main.video-popup')

@if (count($banner) != 0)
    <div class="banner" id="banner" style="width: 100%;">
        @foreach ($banner as $bn)
            <div class="bn-place">
                <div class="bn-image image" style="background-image: url({{ asset('img/banner/covers/'.$bn->cover) }});"></div>
                @if (!empty($bn->title) || !empty($bn->description) || !empty($bn->link))
                    @if ($bn->position == 'center')
                        <?php $post = 'bn-center'; ?>
                    @endif
                    @if ($bn->position == 'bottom')
                        <?php $post = 'bn-bottom'; ?>
                    @endif
                    @if ($bn->position == 'disable')
                        <?php $post = 'bn-disable'; ?>
                    @endif
                    <div class="bn-black"></div>
                    <div class="bn-cover {{ $post }}">
                        <div class="bn-info">
                            <div class="desc">
                                @if (!empty($bn->title))
                                    <h1>
                                        {{ $bn->title }}
                                    </h1>
                                @endif
                                @if (!empty($bn->description))
                                    <p>
                                        {{ $bn->description }}
                                    </p>
                                @endif
                            </div>
                            @if (!empty($bn->link))
                            <div class="more">
                                <a href="{{ $bn->link }}">
                                    <button class="btn btn-radius btn-main-color">
                                        Read More
                                        <span class="fa fa-lg fa-angle-right"></span>
                                    </button>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endif

<div class="other">
    <ul class="menu">
        <li>
            <div class="lef">
                <div class="fa fa-lg fa-calendar-alt"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">
                    Works
                </div>
                <div class="dsc">
                    Everiday : 11.00 am - 11.00 pm
                </div>
            </div>
        </li>
        <li>
            <div class="lef">
                <div class="fa fa-lg fa-phone"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">
                    Contacts & Informations
                </div>
                <div class="dsc">
                    0831-2326-9814
                </div>
            </div>
        </li>
        <li>
            <div class="lef">
                <div class="fa fa-lg fa-map-marker-alt"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">
                    Address
                </div>
                <div class="dsc">
                    Dipatiukur 70B, bandung (beside Neo Hotel)
                </div>
            </div>
        </li>
    </ul>
</div>

@if (count($service) != 0)
<div class="place-bg padding-top-20px">
    <div class="bg-main top"></div>
    <!--konten & layanan-->
    <div class="padding-top-20px"></div>
    <div class="body-block padding-top-20px">
        <div class="top padding-20px">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-white-color padding-20px">
                What we reserve?
            </h1>
        </div>
        <div class="mid center">
            <div class="width width-full width-center wrap">
                @foreach ($service as $sv)
                    @include('web.service.card')
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="padding-20px"></div>

@if (count($galery) != 0)
    <div class="place-bg padding-bottom-20px">
        <div class="bg-main bottom"></div>
        <!--galeri-->
        <div class="body-block">
            <div class="top padding-20px">
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color padding-20px">
                    Our galeries
                </h1>
            </div>
            <div class="mid center">
                <div class="place-more">
                    <div class="cen wrap" id="gc-2">
                    @foreach ($galery as $gl)
                        @include('web.galery.card')
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
        </div>
    </div>
@endif

<div class="footer-map">
    <div class="video" id="video">
        <div 
            class="image"
            style="
                background-image: url( {{ asset('/img/sites/main2.jpg') }});
            "
        ></div>
        <div class="icn fa fa-lg fa-play" onclick="opVideo('show')"></div>
    </div>
</div>

@if (count($menu) != 0)
    <div class="place-bg padding-bottom-20px">
        <div class="bg-main top"></div>
        <div class="body-block">
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="top padding-20px">
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-white-color padding-20px">
                    Our menus
                </h1>
            </div>
            <div class="mid center">
                <div class="place-more">
                    <div class="cen wrap" id="gc-3">
                        @foreach ($menu as $tt)
                            @include('web.menu.card')
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
        </div>
    </div>
@endif

@if (count($article) != 0)
    <div class="place-bg padding-top-20px">
        <div class="bg-main top">
            <div class="padding-20px"></div>
        </div>
        <!--artikel-->
        <div class="body-block">
            <div class="top padding-20px">
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-white-color">
                    News & blogs
                </h1>
            </div>
            <div class="mid center">
                <div class="place-more">
                    <div class="cen wrap" id="gc-1">
                        @foreach ($article as $at)
                            @include('web.article.card-small')
                        @endforeach
                    </div>
                </div>
                <div class="padding-20px"></div>
                <div class="padding-20px"></div>
            </div>
        </div>
    </div>
@endif

@if (count($testimony) != 0)
    <div class="place-bgs">
        <div class="bg-main all"></div>
        <div class="body-block">
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="top padding-20px">
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color padding-20px">
                    What people says
                </h1>
            </div>
            <div class="mid center">
                <div class="place-more">
                    <div class="cen wrap" id="gc-3">
                        @foreach ($testimony as $tt)
                            @include('web.testimonial.card')
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
            <div class="padding-20px"></div>
        </div>
    </div>
@endif

<div class="place-bg padding-top-20px">
    <div class="bg-main bottom"></div>
    <!--konten & layanan-->
    <div class="body-block padding-top-20px">
        <div class="top padding-20px">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color padding-20px">
                Follow us
            </h1>
        </div>
        <div class="mid center">
            <div class="width width-full width-center wrap">
                <div class="frm-service">
                    <a href="https://www.instagram.com/zeroscalecoffee/" class="icn-link" target="_blank">
                        <div class="icn">
                            <span class="fl fab fa-2x fa-instagram"></span>
                        </div>
                    </a>
                </div>
                <div class="frm-service">
                    <a href="#" class="icn-link">
                        <div class="icn">
                            <span class="fl fab fa-2x fa-youtube"></span>
                        </div>
                    </a>
                </div>
                <div class="frm-service">
                    <a href="#" class="icn-link">
                        <div class="icn">
                            <span class="fl fab fa-2x fa-twitter"></span>
                        </div>
                    </a>
                </div>
                <div class="frm-service">
                    <a href="#" class="icn-link">
                        <div class="icn">
                            <span class="fl fab fa-2x fa-facebook"></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="padding-20px"></div>
        <div class="padding-20px"></div>
        <div class="padding-20px"></div>
        <div class="padding-20px"></div>
        <div class="padding-20px"></div>
    </div>
</div>

<div class="place-bg">
    <div class="bg-main all"></div>
    <!--konten & layanan-->
    <div class="body-block">
        <div class="top padding-20px">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-white-color padding-bottom-20px">
                Contact us
            </h1>
        </div>
    
        <div class="mid center padding-bottom-20px">
            <h2 class="ctn-main-font ctn-font-3 ctn-bold ctn-white-color ctn-14pt">
                Phone numbers
            </h2>
            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-white-color ctn-12pt">
                0831-2326-9814
            </p>
        </div>

        <div class="mid center padding-bottom-20px">
            <h2 class="ctn-main-font ctn-font-3 ctn-bold ctn-white-color ctn-14pt">
                Email
            </h2>
            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-white-color ctn-12pt">
                zeroscalecoffee@gmail.com
            </p>
        </div>

        <div class="mid center padding-bottom-20px">
            <h2 class="ctn-main-font ctn-font-3 ctn-bold ctn-white-color ctn-14pt">
                Address
            </h2>
            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-white-color ctn-12pt">
                Jl. Dipati Ukur No.70, Lebakgede, Coblong, Kota Bandung, Jawa Barat 40132
            </p>
        </div>

        <div class="padding-20px"></div>
        <div class="padding-20px"></div>
        <div class="padding-20px"></div>

        <div class="top padding-20px">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-white-color">
                Subscribe
            </h1>
            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-white-color ctn-12pt">
                Subscribe for news, promotions and new menus
            </p>
        </div>

        <div class="padding-10px"></div>

        <div class="mid center small">
            <input 
                type="email" 
                name="email" 
                id="email" 
                style="text-align: center;"
                class="txt txt-main-color"
                placeholder="Your email address"
                value="{{ old('email') }}"
                required="required">
            <div class="padding-10px"></div>
        </div>

    </div>
    <div class="padding-20px"></div>
    <div class="padding-20px"></div>
    <div class="padding-20px"></div>
    <div class="padding-20px"></div>
    <div class="padding-20px"></div>
</div>

<div class="footer-map">
    <div class="map" id="map">
        <div 
            class="image"
            style="
                background-image: url( {{ asset('/img/sites/map.jpg') }});
                cursor: pointer;
            "
            onclick="opMap('show')"
        ></div>
    </div>
</div>

@endsection
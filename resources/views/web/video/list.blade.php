@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top padding-20px">
        <div class="padding-10px"></div>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-small ctn-init-color">
            Video Collections
        </h1>
    </div>
    @if (count($video) != 0)

        <div class="mid center">
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($video as $vid)
                        @include('web.video.card-big')
                    @endforeach
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $video->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Videos still empty
            </h1>
        </div>
    @endif
    <div class="padding-20px"></div>
</div>

@endsection
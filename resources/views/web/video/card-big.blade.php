<?php $path = asset('/vid/covers/'.$vid->cover); ?>
<div 
    onclick="opVideo('show', {{ $vid->idvideo }})"
	class="frm-galery" >
	<div 
        class="image"
        style="background-image: url( {{ asset('/vid/covers/'.$vid->cover) }});"
    >
        <div 
        class="icn fa fa-lg fa-play" 
        onclick="opVideo('show', {{ $vid->idvideo }})"></div>
    </div>
    <div class="desc">
    	<div class="ttl">
            <p>{{ $vid->description }}</p>
        </div>
	</div>
</div>

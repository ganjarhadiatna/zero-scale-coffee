<div class="frm-video">
    <div class="fv-mid">
        <div 
            class="image"
            style="
            background-image: url( {{ asset('/vid/covers/'.$vid->cover) }});
            "
        ></div>
        <div 
            class="icn fa fa-lg fa-play" 
            onclick="opVideo('show', {{ $vid->idvideo }})"></div>
    </div>
</div>
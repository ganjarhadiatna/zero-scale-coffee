<script type="text/javascript">
    var server = "{{ url('') }}";
    function opVideo(stt, idvideo) {
        // var dt = '<iframe width="1000" height="500" src="https://www.youtube.com/embed/EKFN25kMeCE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        if (stt == 'show') {
            $('#video-popup').fadeIn();

            
            $.ajax({
                url: '{{ url("/video/by_id/") }}'+'/'+idvideo,
                dataType: 'json',
                type: 'get'
            })
            .done(function (data) {
                if (data.status == 'success') 
                {
                    var resource = server+'/vid/'+data.video[0].video;
                    var dt = '\
                    <video class="pl-video" controls>\
                        <source src="'+resource+'">\
                    </video>';
                    $('#video-place').html(dt);
                    console.log(dt);
                } 
                else 
                {
                    alert(data.message);
                }
            })
            .fail(function () {
                alert(data.resonseJSON.message);
                // console.log(data.resonseJSON.message);
            })
            .always(function () {

            });
        } 
        else 
        {
            $('#video-popup').fadeOut();
            $('#video-place').html('');
        }
    }
    function opVideoHide () {
        $('#video-popup').fadeOut();
        $('#video-place').html('');
    }
</script>
<div class="frm-popup" id="video-popup">
    <div class="fp-place">
        <div class="close">
            <button 
                class="btn btn-main-color btn-radius" 
                onclick="opVideoHide()">
                <span class="fa fa-lg fa-times"></span>
                <span>Close</span>
            </button>
        </div>
        <div class="fp-mid">
            <div class="vid" id="video-place"></div>
        </div>
    </div>
</div>
<script>
    function clViewImage()
    {
        $('#image-popup').fadeOut();
    }
    function opViewImage(id, path)
    {
        var img = path;
        
        $('#image-popup')
        .fadeIn();

        $('#image-popup')
        .find('.img')
        .attr('src',img);
    }
</script>
<div class="frm-popup" id="image-popup">
    <div class="fp-place">
        <div class="close">
            <button 
                class="btn btn-main-color btn-radius" 
                onclick="clViewImage()">
                <span class="fa fa-lg fa-times"></span>
                <span>Tutup</span>
            </button>
        </div>
        <div class="fp-mid">
            <img src="" class="img">
        </div>
    </div>
</div>
<script type="text/javascript">
	function opMap(stt) 
	{  
        var data = '\
            <iframe \
                src="https://maps.google.com/maps?q=zero%20scale%20coffee&t=&z=13&ie=UTF8&iwloc=&output=embed" \
                width="1000" \
                height="500" \
                frameborder="0" \
                style="border:0" \
                allowfullscreen></iframe>\
        ';
        if (stt == 'show') 
        {
        	$('#map-popup').fadeIn();
        	$('#placeMap').html(data);
        } 
        else 
        {
        	$('#map-popup').fadeOut();
        	$('#placeMap').html('');
        }
    }
</script>
<div class="frm-popup" id="map-popup">
    <div class="fp-place">
        <div class="close">
            <button 
            	class="btn btn-main-color btn-radius" 
            	onclick="opMap('hide')">
                <span class="fa fa-lg fa-times"></span>
                <span>Close</span>
            </button>
        </div>
        <div class="fp-mid">
            <div class="map" id="placeMap"></div>
        </div>
    </div>
</div>
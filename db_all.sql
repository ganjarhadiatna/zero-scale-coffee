-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: db_site
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `idarticle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` blob NOT NULL,
  `type` enum('blog','service') DEFAULT 'blog',
  `pinned` enum('1','0') DEFAULT '0',
  `draft` enum('1','0') DEFAULT '0',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idarticle`),
  KEY `fk_id_article` (`id`),
  CONSTRAINT `fk_id_article` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (9,'115447093592crujfh8blackcoffee625x30024September18.PNG','Lorem ipsum dolor sit amet consectetur adipisicing elit.',_binary '<div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.<br></div>','blog','0','0','2018-12-13 13:56:00',1),(10,'11544709674coffeefoam.jpg.653x0q80cropsmart.jpg','Lorem ipsum dolor sit amet consectetur adipisicing elit.',_binary '<div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</div>','blog','0','0','2018-12-13 14:01:14',1),(11,'11544709740EspressoBeansvsCoffeeBeans.jpg','Lorem ipsum dolor sit amet consectetur adipisicing elit.',_binary '<div style=\"line-height: 16px;\"><span style=\"font-family: Arial;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</span></div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\"><span style=\"font-family: Arial; font-size: 18px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</span></div><div style=\"line-height: 16px;\"><br></div><div style=\"line-height: 16px;\"><span style=\"font-family: Arial; font-size: 18px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.</span></div>','blog','0','0','2018-12-13 14:02:20',1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `idbanner` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(150) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `position` enum('disable','center','bottom') DEFAULT 'center',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idbanner`),
  KEY `id` (`id`),
  CONSTRAINT `fk_id_banner` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (18,'11544708329592f572302004df0909fb1c98d744f5dGettyImages924005570.jpg','Zero Scale Coffee','Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.',NULL,'center','2018-12-13 13:38:50',1);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galery`
--

DROP TABLE IF EXISTS `galery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galery` (
  `idgalery` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idgalery`),
  KEY `fk_id_galery` (`id`),
  CONSTRAINT `fk_id_galery` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galery`
--

LOCK TABLES `galery` WRITE;
/*!40000 ALTER TABLE `galery` DISABLE KEYS */;
INSERT INTO `galery` VALUES (31,'115447086435CoffeeShopdiKelapaGadingyangPerluDikunjungiPecintaKopi.jpg',NULL,'2018-12-13 13:44:03',1),(33,'11544708669coffee.jpg',NULL,'2018-12-13 13:44:29',1),(34,'115447087470717coffeecup.jpg',NULL,'2018-12-13 13:45:47',1),(35,'11544708907whiterosecoffee.jpg',NULL,'2018-12-13 13:48:27',1),(36,'11544708983images.jpeg',NULL,'2018-12-13 13:49:43',1),(38,'11544709104Coffee.jpg',NULL,'2018-12-13 13:51:44',1);
/*!40000 ALTER TABLE `galery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `idnote` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idnote`),
  KEY `fk_id_note` (`id`),
  CONSTRAINT `fk_id_note` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
INSERT INTO `note` VALUES (5,'fa fa-lg fa-map-marker-alt','Avina Lembang','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NA==','2018-09-01 14:41:16',1),(6,'fa fa-lg fa-map-marker-alt','Grafika Cikole Lembang','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NA==','2018-09-01 14:43:47',1),(7,'fa fa-lg fa-map-marker-alt','Cipunagara Subang','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NA==','2018-09-01 14:44:36',1),(8,'fa fa-lg fa-map-marker-alt','The Lodge Maribaya','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NQ==','2018-09-01 14:45:19',1),(9,'fa fa-lg fa-map-marker-alt','Ranca Upas Ciwidey','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NA==','2018-10-18 02:30:08',1),(10,'fa fa-lg fa-map-marker-alt','Kampung Turis','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.','http://localhost:8000/article/NA==','2018-10-18 02:30:24',1);
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `idservice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idservice`),
  KEY `fk_id_service` (`id`),
  CONSTRAINT `fk_id_service` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (2,'fa fa-lg fa-wifi','Free Wi-fi','Lorem ipsum dolor sit amet, consectetur adipisicing elit.',NULL,'2018-08-29 01:38:25',1),(3,'fa fa-lg fa-smoking','Smoking Area','Lorem ipsum dolor sit amet, consectetur adipisicing elit.',NULL,'2018-08-29 01:38:56',1),(6,'fa fa-lg fa-car','Parking Space','Lorem ipsum dolor sit amet, consectetur adipisicing elit.',NULL,'2018-10-18 01:16:22',1),(7,'fa fa-lg fa-cogs','Espresso Machine','Lorem ipsum dolor sit amet, consectetur adipisicing elit.',NULL,'2018-12-18 13:29:06',1);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `idtags` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(250) NOT NULL,
  `idgalery` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idtags`),
  KEY `fk_idgalery_tags` (`idgalery`),
  CONSTRAINT `fk_idgalery_tags` FOREIGN KEY (`idgalery`) REFERENCES `galery` (`idgalery`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimony`
--

DROP TABLE IF EXISTS `testimony`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimony` (
  `idtestimony` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response` varchar(250) NOT NULL,
  `name` varchar(50) NOT NULL,
  `job` varchar(250) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idtestimony`),
  KEY `fk_id_testimony` (`id`),
  CONSTRAINT `fk_id_testimony` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimony`
--

LOCK TABLES `testimony` WRITE;
/*!40000 ALTER TABLE `testimony` DISABLE KEYS */;
INSERT INTO `testimony` VALUES (7,'Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.','Shinta Naomi','Mahasiswa','115447098071.jpeg','2018-12-13 14:03:27',1),(8,'Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.','Alexandra','Actress','115447098921f99b0ec6c03a9c9e6d6bba0b6af9557.jpg','2018-12-13 14:04:53',1),(9,'Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.','Margot','Actress','115447099373da8254c181a1a01a5c228f31ee34fe6.jpg','2018-12-13 14:05:37',1),(10,'Voluptate, molestias velit officiis, illum consequatur, iste nesciunt inventore ullam dolore porro unde nisi quod totam adipisci soluta aliquam vitae itaque debitis.','Minhee','Singer','115447099767d30a8f5140845ee14e880a374973170.jpg','2018-12-13 14:06:16',1);
/*!40000 ALTER TABLE `testimony` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ganjar Hadiatna','ganjardbc@gmail.com','ganjardbc','$2y$10$3ccGFOSsWwabOr4X2HTbiu7HmbP7EN7ldUpz6jHXp4pVQj8mtYh9e','115358735823905153621285484307407156209280154502430720n.jpg','WxLN5sEp5nQfUFgOuYGUi6TcwyCRYU7Pp4PeAbcKnLoDyeHj6cWwlxh8Kyes','2018-08-25 22:59:55','2018-08-25 22:59:55'),(2,'Admin','admin@gmail.com','admin','$2y$10$N7bZ5u1CfHe2yF9KxI9bMO3FPLqScSVFMvm8E59JxwxroYauJotWa',NULL,NULL,'2018-08-29 23:49:46','2018-08-29 23:49:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-21 19:45:06

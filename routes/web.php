<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//web
Route::get('*', function () {
    return view('404', [
        'title' => '404 Not Found',
        'path' => 'none'
    ]);
});

Route::get('/', 'WebController@index');
Route::get('/instagram', 'WebController@instagram');
Route::get('/search', 'WebController@search');

//service
Route::get('/service/{ctr}', 'ServiceController@list');

//article
Route::get('/article/{id}', 'ArticleController@view');
Route::get('/articles', 'ArticleController@list');

//galery
Route::get('/galeries', 'GaleryController@list');
Route::get('/galeries/tags/{tag}', 'GaleryController@tags');
Route::get('/galery/{idgalery}', 'GaleryController@view');

//menu
Route::get('/menus', 'MenuController@list');
//Route::get('/menus/tags/{tag}', 'MenuController@tags');
Route::get('/menu/{idmenu}', 'MenuController@view');

//banner

//video
Route::get('/videos', 'VideoController@list');
Route::get('/video/by_id/{id}', 'VideoController@byId');

//sites
Route::get('/sites/about-us', 'WebController@aboutUs');
Route::get('/sites/terms-n-conditions', 'WebController@tnc');
Route::get('/sites/privacy', 'WebController@privacy');
Route::get('/sites/faq', 'WebController@faq');

//admin
//Auth::routes();
Route::get('/punten', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

//private
Route::middleware('auth')->group(function() {
    Route::get('/home', 'WebController@admin');
});

Route::middleware('auth')->prefix('admin')->group(function() {
    
    Route::get('/', 'WebController@admin');

    //banner
    Route::get('/banner', 'BannerController@index');
    Route::get('/banner/create', 'BannerController@create');
    Route::get('/banner/edit/{id}', 'BannerController@edit');
    Route::post('/banner/publish', 'BannerController@publish');
    Route::post('/banner/put', 'BannerController@put');
    Route::post('/banner/remove', 'BannerController@remove');
    Route::post('/banner/changePosition', 'BannerController@changePosition');

    //service
    Route::get('/service', 'ServiceController@index');
    Route::get('/service/create', 'ServiceController@create');
    Route::get('/service/edit/{id}', 'ServiceController@edit');
    Route::post('/service/publish', 'ServiceController@publish');
    Route::post('/service/put', 'ServiceController@put');
    Route::post('/service/remove', 'ServiceController@remove');

    //article
    Route::get('/article', 'ArticleController@index');
    Route::get('/article/create', 'ArticleController@create');
    Route::get('/article/edit/{id}', 'ArticleController@edit');
    Route::post('/article/publish', 'ArticleController@publish');
    Route::post('/article/put', 'ArticleController@put');
    Route::post('/article/remove', 'ArticleController@remove');

    //draft
    Route::post('/article/changePinned', 'ArticleController@changePinned');
    Route::post('/article/changeDraft', 'ArticleController@changeDraft');

    //galery
    Route::get('/galery', 'GaleryController@index');
    Route::get('/galery/create', 'GaleryController@create');
    Route::get('/galery/edit/{id}', 'GaleryController@edit');
    Route::post('/galery/publish', 'GaleryController@publish');
    Route::post('/galery/put', 'GaleryController@put');
    Route::post('/galery/remove', 'GaleryController@remove');

    //menu
    Route::get('/menu', 'MenuController@index');
    Route::get('/menu/create', 'MenuController@create');
    Route::get('/menu/edit/{id}', 'MenuController@edit');
    Route::post('/menu/publish', 'MenuController@publish');
    Route::post('/menu/put', 'MenuController@put');
    Route::post('/menu/remove', 'MenuController@remove');

    //video
    Route::get('/video', 'VideoController@index');
    Route::get('/video/create', 'VideoController@create');
    Route::get('/video/edit/{id}', 'VideoController@edit');
    Route::post('/video/publish', 'VideoController@publish');
    Route::post('/video/put', 'VideoController@put');
    Route::post('/video/remove', 'VideoController@remove');

    //note
    Route::get('/note', 'NoteController@index');
    Route::get('/note/create', 'NoteController@create');
    Route::get('/note/edit/{id}', 'NoteController@edit');
    Route::post('/note/publish', 'NoteController@publish');
    Route::post('/note/put', 'NoteController@put');
    Route::post('/note/remove', 'NoteController@remove');

    //testimony
    Route::get('/testimony', 'TestimonyController@index');
    Route::get('/testimony/create', 'TestimonyController@create');
    Route::get('/testimony/edit/{id}', 'TestimonyController@edit');
    Route::post('/testimony/publish', 'TestimonyController@publish');
    Route::post('/testimony/put', 'TestimonyController@put');
    Route::post('/testimony/remove', 'TestimonyController@remove');

    //admin
    Route::get('/admin', 'AdminController@index')->name('listAdmin');
    Route::get('/admin/create', 'AdminController@create')->name('createAdmin');
    Route::get('/admin/edit', 'AdminController@edit');
    Route::get('/admin/password', 'AdminController@password');
    Route::post('/admin/publish', 'AdminController@publish');
    Route::post('/admin/photo', 'AdminController@photo');
    Route::post('/admin/put', 'AdminController@put');
    Route::post('/admin/password', 'AdminController@password');
    Route::post('/admin/remove', 'AdminController@remove');

    //pin
    Route::post('/pin/add', 'PinController@add');
    Route::post('/pin/remove', 'PinController@remove');
});
<?php

namespace Adventrest\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use Adventrest\NoteModel;

class NoteController extends Controller
{
    //private
    function index()
    {
        $note = NoteModel::AllNote(10);
        return view('admin.note.index', [
            'path' => 'note',
            'note' => $note
        ]);
    }
    function create()
    {
        return view('admin.note.create', [
            'path' => 'note'
        ]);
    }
    function edit($idnote)
    {
        $note = NoteModel::NoteById($idnote);
        return view('admin.note.edit', [
            'path' => 'note',
            'note' => $note
        ]);
    }

    //public
    function list($idnote)
    {
        $note = NoteModel::NoteById(base64_decode($idnote));
        return view('web.note.index', [
            'note' => $note
        ]);
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $icon = $req['icon'];
            $title = $req['title'];
            $description = $req['description'];
            $link = $req['link'];

            $data = [
                'icon' => $icon,
                'title' => $title,
                'description' => $description,
                'link' => $link,
                'id' => $id
            ];

            $sql = NoteModel::Insert($data);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Publish note success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Publish note failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }
    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idnote = $req['idnote'];
            $icon = $req['icon'];
            $title = $req['title'];
            $description = $req['description'];
            $link = $req['link'];

            $data = [
                'icon' => $icon,
                'title' => $title,
                'description' => $description,
                'link' => $link,
                'id' => $id
            ];

            $sql = NoteModel::Edit($data, $idnote);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited note success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Edited note failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idnote = $req['idnote'];

            //remove database
            $sql = NoteModel::Remove($idnote);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Delete note success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete note failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}

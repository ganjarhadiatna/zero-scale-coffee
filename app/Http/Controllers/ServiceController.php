<?php

namespace Adventrest\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;

use Adventrest\ServiceModel;
use Adventrest\ArticleModel;

class ServiceController extends Controller
{
    //private
    function index()
    {
        $service = ServiceModel::AllService(10);
        return view('admin.service.index', [
            'path' => 'service',
            'service' => $service
        ]);
    }
    function create()
    {
        return view('admin.service.create', [
            'path' => 'service'
        ]);
    }
    function edit($idservice)
    {
        $service = ServiceModel::ServiceById($idservice);
        return view('admin.service.edit', [
            'path' => 'service',
            'service' => $service
        ]);
    }

    //public
    function list($idservice)
    {
        $words = ucwords(urldecode($idservice));
        $id = ServiceModel::GetIdService($words);
        $article = ArticleModel::AllArticleService(10, $id, 'desc');
        $service = ServiceModel::ServiceById($id);
        return view('web.service.index', [
            'path' => 'service',
            'title' => config('app.name').' - '.$words,
            'service' => $service,
            'article' => $article
        ]);
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $icon = $req['icon'];
            $title = $req['title'];
            $description = $req['description'];
            $link = $req['link'];

            $data = [
                'icon' => $icon,
                'title' => $title,
                'description' => $description,
                'link' => $link,
                'id' => $id
            ];

            $sql = ServiceModel::Insert($data);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Publish service success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Publish service failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }
    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idservice = $req['idservice'];
            $icon = $req['icon'];
            $title = $req['title'];
            $description = $req['description'];
            $link = $req['link'];

            $data = [
                'icon' => $icon,
                'title' => $title,
                'description' => $description,
                'link' => $link,
                'id' => $id
            ];

            $sql = ServiceModel::Edit($data, $idservice);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited service success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Edited service failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idservice = $req['idservice'];

            //remove database
            $sql = ServiceModel::Remove($idservice);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Delete service success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete service failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}

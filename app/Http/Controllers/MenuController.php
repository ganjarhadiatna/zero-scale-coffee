<?php

namespace Adventrest\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use Adventrest\MenuModel;

class MenuController extends Controller
{
    //private
    function index()
    {
        $menu = MenuModel::AllMenu(12, 'desc');
        return view('admin.menu.index', [
            'path' => 'menu',
            'title' => 'Daftar Menu',
            'menu' => $menu
        ]);
    }
    function create()
    {
        return view('admin.menu.create', [
            'path' => 'menu'
        ]);
    }
    function edit($idmenu)
    {
        $menu = MenuModel::menuById($idmenu);
        return view('admin.menu.edit', [
            'path' => 'menu',
            'menu' => $menu
        ]);
    }

    //public
    function list()
    {
        $menu = MenuModel::AllMenu(12, 'desc');
        return view('web.menu.list', [
            'path' => 'menu',
            'title' => config('app.name').' - our activites and menus',
            'menu' => $menu
        ]);
    }
    function view($idmenu)
    {
        $id = base64_decode($idmenu);
        $detail = MenuModel::DetailMenu($id);
        $menu = MenuModel::AllMenu(12, 'desc');
        $tags = TagModel::GetTagsById($id);
        return view('web.menu.view', [
            'path' => 'menu',
            'title' => 'Daftar Menu',
            'detail' => $detail,
            'menu' => $menu,
            'tags' => $tags
        ]);
    }
    function tags($tag)
    {
        $menu = TagModel::GetmenuByTag(4, $tag);
        $tags = TagModel::GetTopTags(12);
        return view('web.menu.list', [
            'path' => 'menu',
            'title' => 'Daftar Menu',
            'menu' => $menu,
            'tags' => $tags
        ]);
    }

    //funtion
    function mentions($tags, $idmenu)
    {
        $replace = array(
            '[',']','@','+','-','*','<','>',
            '-','(',')',';','&','%','$','!',
            '`','~','=','{','}','/',':','?',
            '"',"'",'^'
        );
        $str1 = str_replace($replace, '', $tags);
        $str2 = str_replace(array(', ', ' , ', ' ,'), ',', $str1);
        $tag = explode(',', $str2);
        $count_tag = count($tag);

        for ($i = 0; $i < $count_tag; $i++) {
            if ($tag[$i] != '') {
                $data = array([
                    'tag' => $tag[$i],
                    'idmenu' => $idmenu
                ]);
                TagModel::Insert($data);
            }
        }
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            if ($req->hasFile('cover')) 
            {
                $checkFile = $this->validate($req, [
                    'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:50000',
                ]);

                if ($checkFile) 
                {
                    $cover = $req->file('cover');
        
                    $chrc = array(
                        '[',']','@',' ','+','-','#','*',
                        '<','>','_','(',')',';',',','&',
                        '%','$','!','`','~','=','{','}',
                        '/',':','?','"',"'",'^'
                    );
                    $filename = $id.time().str_replace($chrc, '', $cover->getClientOriginalName());
        
                    //create thumbnail
                    $destination = public_path('img/menu/thumbnails/'.$filename);
                    $img = Image::make($cover->getRealPath());

                    $thumbnail = $img->resize(600, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destination);
        
                    //create image real
                    $destination = public_path('img/menu/covers/');
                    $real = $cover->move($destination, $filename); 

                    if ($thumbnail && $real) 
                    {
                        $cvrName = $filename;
                        // $tags = $req['tags'];

                        $data = [
                            'cover' => $cvrName,
                            'title' => $req['title'],
                            'description' => $req['description'],
                            'price' => $req['price'],
                            'id' => $id
                        ];

                        $sql = MenuModel::Insert($data);
                        if ($sql) 
                        {
                            // $idmenu = MenuModel::GetmenuRecentId();
                            // $this->mentions($tags, $idmenu);

                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload menu success',
                            ]);
                        } 
                        else 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload menu failed',
                            ]);
                        }

                    } 
                    else 
                    {
                        return json_encode([
                            'status' => 'error',
                            'message' => 'Upload cover failed',
                        ]);
                    }
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Cover not valid',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Please choose one cover',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            // $tags = $req['tags'];
            $idmenu = $req['idmenu'];

            $data = [
                'title' => $req['title'],
                'description' => $req['description'],
                'price' => $req['price'], 
                'id' => $id
            ];

            //remove tags
            // TagModel::RemoveByIdMenu($idmenu);

            //add mentions
            // $this->mentions($tags, $idmenu);

            $sql = MenuModel::Edit($data, $idmenu);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited menu success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited menu failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idmenu = $req['idmenu'];

            //getting image
            $image = MenuModel::menuImageById($idmenu);

            if (true) 
            {
                //remove database
                $sql = MenuModel::Remove($idmenu);
                if ($sql) 
                {
                    //remove image
                    $rmvThumbnail = unlink(public_path('img/menu/thumbnails/'.$image));
                    $rmvCover = unlink(public_path('img/menu/covers/'.$image));
                    return json_encode([
                        'status' => 'success',
                        'message' => 'Delete menu success',
                    ]);
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Delete menu failed',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete cover failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}

<?php

namespace Adventrest\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use Adventrest\VideoModel;

class VideoController extends Controller
{
    //private
    function index()
    {
        $video = VideoModel::AllVideo(12, 'desc');
        return view('admin.video.index', [
            'path' => 'video',
            'title' => 'Daftar video',
            'video' => $video
        ]);
    }
    function create()
    {
        return view('admin.video.create', [
            'path' => 'video'
        ]);
    }
    function edit($idvideo)
    {
        $video = VideoModel::videoById($idvideo);
        return view('admin.video.edit', [
            'path' => 'video',
            'video' => $video
        ]);
    }

    //public
    function byId($idvideo)
    {
        $video = VideoModel::videoById($idvideo);
        if ($video) 
        {
            return json_encode([
                'status' => 'success',
                'video' => $video
            ]);
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'we can not get this video'
            ]);
        }

    }
    function list()
    {
        $video = VideoModel::AllVideo(12, 'desc');
        return view('web.video.list', [
            'path' => 'video',
            'title' => config('app.name').' - our activites and videos',
            'video' => $video,
        ]);
    }
    function view($idvideo)
    {
        $id = base64_decode($idvideo);
        $detail = VideoModel::DetailVideo($id);
        $video = VideoModel::AllVideo(12, 'desc');
        $tags = TagModel::GetTagsById($id);
        return view('web.video.view', [
            'path' => 'video',
            'title' => 'Daftar video',
            'detail' => $detail,
            'video' => $video,
            'tags' => $tags
        ]);
    }
    function tags($tag)
    {
        $video = TagModel::GetvideoByTag(4, $tag);
        $tags = TagModel::GetTopTags(12);
        return view('web.video.list', [
            'path' => 'video',
            'title' => 'Daftar video',
            'video' => $video,
            'tags' => $tags
        ]);
    }

    //funtion
    function mentions($tags, $idvideo)
    {
        $replace = array(
            '[',']','@','+','-','*','<','>',
            '-','(',')',';','&','%','$','!',
            '`','~','=','{','}','/',':','?',
            '"',"'",'^'
        );
        $str1 = str_replace($replace, '', $tags);
        $str2 = str_replace(array(', ', ' , ', ' ,'), ',', $str1);
        $tag = explode(',', $str2);
        $count_tag = count($tag);

        for ($i = 0; $i < $count_tag; $i++) {
            if ($tag[$i] != '') {
                $data = array([
                    'tag' => $tag[$i],
                    'idvideo' => $idvideo
                ]);
                TagModel::Insert($data);
            }
        }
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            if ($req->hasFile('cover') || $req->hasFile('video')) 
            {
                $checkFile = $this->validate($req, [
                    'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:50000',
                ]);

                if ($checkFile) 
                {
                    // Upload image or cover
                    $cover = $req->file('cover');

                    $chrc = array(
                        '[',']','@',' ','+','-','#','*',
                        '<','>','_','(',')',';',',','&',
                        '%','$','!','`','~','=','{','}',
                        '/',':','?','"',"'",'^'
                    );
                    $filename = $id.time().str_replace($chrc, '', $cover->getClientOriginalName());
        
                    //create thumbnail
                    $destination = public_path('vid/thumbnails/'.$filename);
                    $img = Image::make($cover->getRealPath());

                    $thumbnail = $img->resize(600, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destination);
        
                    //create image real
                    $destination = public_path('vid/covers/');
                    $real = $cover->move($destination, $filename); 




                    // Upload video
                    $video = $req->file('video');
                    $chrc = array(
                        '[',']','@',' ','+','-','#','*',
                        '<','>','_','(',')',';',',','&',
                        '%','$','!','`','~','=','{','}',
                        '/',':','?','"',"'",'^'
                    );
                    $filenameVideo = $id.time().str_replace($chrc, '', $video->getClientOriginalName());

                    $destinationVideo = public_path('vid/');
                    $realVideo = $video->move($destinationVideo, $filenameVideo); 




                    if ($thumbnail && $real && $realVideo) 
                    {
                        $cvrName = $filename;
                        $vidName = $filenameVideo;
                        // $tags = $req['tags'];

                        $data = [
                            'cover' => $cvrName,
                            'video' => $vidName,
                            'description' => $req['description'],
                            'id' => $id
                        ];

                        $sql = VideoModel::Insert($data);
                        if ($sql) 
                        {
                            // $idvideo = VideoModel::GetvideoRecentId();
                            // $this->mentions($tags, $idvideo);

                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload video success',
                            ]);
                        } 
                        else 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload video failed',
                            ]);
                        }

                    } 
                    else 
                    {
                        return json_encode([
                            'status' => 'error',
                            'message' => 'Upload cover failed',
                        ]);
                    }
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Cover not valid',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Please choose one cover and video',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }

    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            // $tags = $req['tags'];
            $idvideo = $req['idvideo'];

            $data = [
                'description' => $req['description'],
                'id' => $id
            ];

            //remove tags
            // TagModel::RemoveByIdVideo($idvideo);

            //add mentions
            // $this->mentions($tags, $idvideo);

            $sql = VideoModel::Edit($data, $idvideo);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited video success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited video failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idvideo = $req['idvideo'];

            //getting image
            $image = VideoModel::videoImageById($idvideo);
            $video = VideoModel::videoVideoById($idvideo);

            if ($image && $video) 
            {
                //remove database
                $sql = VideoModel::Remove($idvideo);
                if ($sql) 
                {
                    //remove image
                    $rmvThumbnail = unlink(public_path('vid/thumbnails/'.$image));
                    $rmvCover = unlink(public_path('vid/covers/'.$image));

                    //remove video
                    $rmvVideo = unlink(public_path('vid/'.$video));

                    return json_encode([
                        'status' => 'success',
                        'message' => 'Delete video success',
                    ]);
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Delete video failed',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete cover failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}

<?php

namespace Adventrest;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MenuModel extends Model
{
    protected $table = 'menu';

    //create
    function scopeInsert($quer, $data)
    {
        return DB::table($this->table)
        ->insert($data);
    }

    //update
    function scopeEdit($quer, $data, $idmenu)
    {
        return DB::table($this->table)
        ->where('menu.idmenu', $idmenu)
        ->update($data);
    }

    //delete
    function scopeRemove($quer, $idmenu)
    {
        return DB::table($this->table)
        ->where('menu.idmenu', $idmenu)
        ->delete();
    }

    //read
    function scopeGetmenuRecentId($query)
    {
        return DB::table($this->table)
        ->orderBy('menu.idmenu', 'desc')
        ->limit(1)
        ->value('menu.idmenu');
    }
    function scopemenuImageById($query, $idmenu)
    {
        return DB::table($this->table)
        ->where('menu.idmenu', $idmenu)
        ->value('menu.cover');
    }
    function scopeDetailMenu($query, $idmenu)
    {
        return DB::table($this->table)
        ->select(
            'menu.idmenu',
            'menu.cover',
            'menu.title',
            'menu.price',
            'menu.description',
            'menu.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->where('menu.idmenu', $idmenu)
        ->join('users','users.id', '=', 'menu.id')
        ->get();
    }
    function scopeAllMenu($query, $limit, $order = 'asc')
    {
        return DB::table($this->table)
        ->select(
            'menu.idmenu',
            'menu.cover',
            'menu.title',
            'menu.price',
            'menu.description',
            'menu.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'menu.id')
        ->orderBy('menu.idmenu', $order)
        ->paginate($limit);
    }
    function scopeSearchMenu($query, $ctr, $limit, $order = 'asc')
    {
        $searchValues = preg_split('/\s+/', $ctr, -1, PREG_SPLIT_NO_EMPTY);
        return DB::table($this->table)
        ->select(
            'menu.idmenu',
            'menu.cover',
            'menu.title',
            'menu.price',
            'menu.description',
            'menu.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'menu.id')
        ->where('menu.title','like',"%$ctr%")
        ->orWhere('menu.description','like',"%$ctr%")
        ->orWhere('menu.price','like',"%$ctr%")
        ->orWhere(function ($q) use ($searchValues)
        {
            foreach ($searchValues as $value) {
                $q->orWhere('menu.title','like',"%$value%");
                $q->orWhere('menu.description','like',"%$value%");
                $q->orWhere('menu.price','like',"%$value%");
            }
        })
        ->orderBy('menu.idmenu', $order)
        ->paginate($limit);
    }
    function scopemenuById($query, $idmenu)
    {
        return DB::table($this->table)
        ->select(
            'menu.idmenu',
            'menu.cover',
            'menu.title',
            'menu.price',
            'menu.description',
            'menu.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'menu.id')
        ->where('menu.idmenu', $idmenu)
        ->get();
    }
}

<?php

namespace Adventrest;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VideoModel extends Model
{
    protected $table = 'video';

    //create
    function scopeInsert($quer, $data)
    {
        return DB::table($this->table)
        ->insert($data);
    }

    //update
    function scopeEdit($quer, $data, $idvideo)
    {
        return DB::table($this->table)
        ->where('video.idvideo', $idvideo)
        ->update($data);
    }

    //delete
    function scopeRemove($quer, $idvideo)
    {
        return DB::table($this->table)
        ->where('video.idvideo', $idvideo)
        ->delete();
    }

    //read
    function scopeGetvideoRecentId($query)
    {
        return DB::table($this->table)
        ->orderBy('video.idvideo', 'desc')
        ->limit(1)
        ->value('video.idvideo');
    }
    function scopevideoImageById($query, $idvideo)
    {
        return DB::table($this->table)
        ->where('video.idvideo', $idvideo)
        ->value('video.cover');
    }
    function scopevideoVideoById($query, $idvideo)
    {
        return DB::table($this->table)
        ->where('video.idvideo', $idvideo)
        ->value('video.video');
    }
    function scopeDetailVideo($query, $idvideo)
    {
        return DB::table($this->table)
        ->select(
            'video.idvideo',
            'video.cover',
            'video.video',
            'video.description',
            'video.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->where('video.idvideo', $idvideo)
        ->join('users','users.id', '=', 'video.id')
        ->get();
    }
    function scopeAllVideo($query, $limit, $order = 'asc')
    {
        return DB::table($this->table)
        ->select(
            'video.idvideo',
            'video.cover',
            'video.video',
            'video.description',
            'video.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'video.id')
        ->orderBy('video.idvideo', $order)
        ->paginate($limit);
    }
    function scopeSearchVideo($query, $ctr, $limit, $order = 'asc')
    {
        $searchValues = preg_split('/\s+/', $ctr, -1, PREG_SPLIT_NO_EMPTY);
        return DB::table($this->table)
        ->select(
            'video.idvideo',
            'video.cover',
            'video.video',
            'video.description',
            'video.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'video.id')
        ->where('video.description','like',"%$ctr%")
        ->orWhere(function ($q) use ($searchValues)
        {
            foreach ($searchValues as $value) {
                $q->orWhere('video.description','like',"%$value%");
            }
        })
        ->orderBy('video.idvideo', $order)
        ->paginate($limit);
    }
    function scopevideoById($query, $idvideo)
    {
        return DB::table($this->table)
        ->select(
            'video.idvideo',
            'video.cover',
            'video.video',
            'video.description',
            'video.date',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'video.id')
        ->where('video.idvideo', $idvideo)
        ->get();
    }
}

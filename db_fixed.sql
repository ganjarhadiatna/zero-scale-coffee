-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: db_zero
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `idarticle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` blob NOT NULL,
  `type` enum('blog','service') DEFAULT 'blog',
  `pinned` enum('1','0') DEFAULT '0',
  `draft` enum('1','0') DEFAULT '0',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idarticle`),
  KEY `fk_id_article` (`id`),
  CONSTRAINT `fk_id_article` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (12,'11546057042EspressoBeansvsCoffeeBeans.jpg','Lorem ipsum dolor sit amet consectetur adipisicing elit.','<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p>','blog','0','0','2018-12-29 04:17:23',1),(13,'11546057261548746620.jpg','Lorem ipsum dolor sit amet consectetur adipisicing elit.','<div class=\"ctn-main-font ctn-14pt ctn-sek-color ctn-font-3\">\r\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p>                </div>','blog','0','0','2018-12-29 04:21:01',1),(14,'11546057282coffee.jpg','Lorem ipsum dolor sit amet consectetur adipisicing elit.','<div class=\"ctn-main-font ctn-14pt ctn-sek-color ctn-font-3\">\r\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. \r\n					Ad officiis vitae voluptas natus, amet vel laborum adipisci \r\n					veritatis asperiores itaque debitis suscipit ab alias, \r\n					perspiciatis repudiandae nam minima quis ex?\r\n				</p>                </div>','blog','0','0','2018-12-29 04:21:22',1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `idbanner` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(150) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `position` enum('disable','center','bottom') DEFAULT 'center',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idbanner`),
  KEY `id` (`id`),
  CONSTRAINT `fk_id_banner` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (19,'1154605652520180314191320.jpg','Zero Scale Coffee','Tempat Untuk Merasakan Rasa Kopi Yang Sesungguhnya','http://localhost:8000/sites/about-us','center','2018-12-29 04:08:49',1),(20,'11546059505297171951972297375470474475750333573758976n.jpg','Promo 12 Ribu','Hai Coffee lovers jangan lupa mampir ke tempat kita dan coba varian kopi kita.. ada yang lagi promo cuma 12rb lohh..harganya oke kaann..',NULL,'center','2018-12-29 04:58:25',1);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galery`
--

DROP TABLE IF EXISTS `galery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galery` (
  `idgalery` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idgalery`),
  KEY `fk_id_galery` (`id`),
  CONSTRAINT `fk_id_galery` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galery`
--

LOCK TABLES `galery` WRITE;
/*!40000 ALTER TABLE `galery` DISABLE KEYS */;
INSERT INTO `galery` VALUES (39,'11546057725355748393984153773173715219193139398443008n.jpg','Vanila','2018-12-29 04:28:45',1),(40,'11546057839317524782252352149060621807996552317239296n.jpg','Monster Milo','2018-12-29 04:30:39',1),(41,'11546057927309540385747688562121055101966916201742336n.jpg','Matcha Choco','2018-12-29 04:32:08',1),(42,'11546059065250057731627033011287662510125646814904320n.jpg','Thai Tea with Espresso Latte','2018-12-29 04:51:07',1),(43,'115460591982607115416824785651513037456793314461745152n.jpg','Pink Macha','2018-12-29 04:53:18',1),(44,'11546059579300871117880324947257763504939110219382784n.jpg','Strawberry milk coffee for ur Saturday! Happy weekend!','2018-12-29 04:59:39',1),(46,'11546060066300770001670752773247164941802526262427648n.jpg','One of our favorite, Ice Coffee Aren!','2018-12-29 05:07:46',1),(47,'11546060275305918921922651315727913851067771529986048n.jpg','Siapa sih yg ga suka chocolate? Kayanya hampir semua orang suka deh ya. Ice choco latte emang cocok buat semua kalangan sih. Yuk diorder yu cuma 15k aja!!','2018-12-29 05:11:15',1);
/*!40000 ALTER TABLE `galery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `idnote` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idnote`),
  KEY `fk_id_note` (`id`),
  CONSTRAINT `fk_id_note` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `idservice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idservice`),
  KEY `fk_id_service` (`id`),
  CONSTRAINT `fk_id_service` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (8,'fa fa-lg fa-wifi','Free Wifi','Lorem ipsum dolor sit amet consectetur adipisicing elit.',NULL,'2018-12-29 04:11:59',1),(9,'fa fa-lg fa-smoking','Smoking Area','Lorem ipsum dolor sit amet consectetur adipisicing elit.',NULL,'2018-12-29 04:13:24',1),(10,'fa fa-lg fa-car','Parking Space','Lorem ipsum dolor sit amet consectetur adipisicing elit.',NULL,'2018-12-29 04:13:58',1),(11,'fa fa-lg fa-cogs','Espresso Machine','Lorem ipsum dolor sit amet consectetur adipisicing elit.',NULL,'2018-12-29 04:14:19',1);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `idtags` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(250) NOT NULL,
  `idgalery` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idtags`),
  KEY `fk_idgalery_tags` (`idgalery`),
  CONSTRAINT `fk_idgalery_tags` FOREIGN KEY (`idgalery`) REFERENCES `galery` (`idgalery`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'menu',39),(2,'menu',40),(3,'menu',41),(4,'menu',42),(5,'menu',43);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimony`
--

DROP TABLE IF EXISTS `testimony`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimony` (
  `idtestimony` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response` varchar(250) NOT NULL,
  `name` varchar(50) NOT NULL,
  `job` varchar(250) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idtestimony`),
  KEY `fk_id_testimony` (`id`),
  CONSTRAINT `fk_id_testimony` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimony`
--

LOCK TABLES `testimony` WRITE;
/*!40000 ALTER TABLE `testimony` DISABLE KEYS */;
INSERT INTO `testimony` VALUES (11,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad officiis vitae voluptas natus, amet vel laborum adipisci veritatis asperiores itaque debitis suscipit ab alias, perspiciatis repudiandae nam minima quis ex?','Lorem','Lorem','11546057555201843417544031347305422204731937103282176n.jpg','2018-12-29 04:25:56',1),(12,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad officiis vitae voluptas natus, amet vel laborum adipisci veritatis asperiores itaque debitis suscipit ab alias, perspiciatis repudiandae nam minima quis ex?','Lorem 2','Lorem','11546057580201843417544031347305422204731937103282176n.jpg','2018-12-29 04:26:21',1),(13,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad officiis vitae voluptas natus, amet vel laborum adipisci veritatis asperiores itaque debitis suscipit ab alias, perspiciatis repudiandae nam minima quis ex?','Lorem 3','Lorem','11546057602201843417544031347305422204731937103282176n.jpg','2018-12-29 04:26:42',1),(14,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad officiis vitae voluptas natus, amet vel laborum adipisci veritatis asperiores itaque debitis suscipit ab alias, perspiciatis repudiandae nam minima quis ex?','Lorem 4','Lorem','11546057617201843417544031347305422204731937103282176n.jpg','2018-12-29 04:26:58',1);
/*!40000 ALTER TABLE `testimony` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ganjar Hadiatna','ganjardbc@gmail.com','ganjardbc','$2y$10$3ccGFOSsWwabOr4X2HTbiu7HmbP7EN7ldUpz6jHXp4pVQj8mtYh9e','115358735823905153621285484307407156209280154502430720n.jpg','WxLN5sEp5nQfUFgOuYGUi6TcwyCRYU7Pp4PeAbcKnLoDyeHj6cWwlxh8Kyes','2018-08-25 15:59:55','2018-08-25 15:59:55'),(2,'Admin','admin@gmail.com','admin','$2y$10$N7bZ5u1CfHe2yF9KxI9bMO3FPLqScSVFMvm8E59JxwxroYauJotWa',NULL,NULL,'2018-08-29 16:49:46','2018-08-29 16:49:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-29 13:34:53
